#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	int val;
	struct Node* next;
}Node;

typedef struct List
{
	Node* head;
}List;

void initializeList(List* list)
{
	list->head = NULL;
}

void print(List list, FILE* file)
{
	for (Node* c = list.head; c; c = c->next)
		printf("%i ", c->val);
}

void print(List list)
{
	print(list, stdout);
}

Node* CreateNode(int val, Node* next)
{
	Node* node = (Node*)malloc(sizeof(Node));
	node->val = val;
	node->next = next;
	return node;
}

Node* CreateNode(int val)
{
	return CreateNode(val, NULL);
}

void prepend(List* list, Node* node)
{
	node->next = list->head;
	list->head = node;
}

void append(List* list, Node* node)
{
	if (list->head == NULL)
	{
		list->head = node;
		return;
	}

	Node* last = list->head;
	while (last->next)
		last = last->next;
	last->next = node;
}

void insertSorted(List* list, Node* node) //insert when null and insert in the middle cases are done
//to be done: last when null +when not null and insert beginning when ist not null
{
	if (!list->head)
	{
		prepend(list, node);
		return;
	}

	Node* p = list->head;
	Node* c = NULL;

	while (node->val > p->val)
	{
		c = p;
		p = p->next;
	}

	c->next = node;
	node->next = p;
}

int main()
{
	List list;
	initializeList(&list);
	prepend(&list, CreateNode(6));
	prepend(&list, CreateNode(1));
	append(&list, CreateNode(100));
	insertSorted(&list, CreateNode(-1));
	print(list);

	char c = getchar();
	return 0;
}