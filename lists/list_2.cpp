#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define D 32
typedef struct Resource
{
	int id,addr;
	char description[D];
	int byte_size;
	unsigned int state:1;
	struct Resource* next;
}Resource;

typedef struct List
{
	Resource* head;
}List;

void initializeList(List* list)
{
	list->head = NULL;
}

//----------------------------------------------------------
void initializeList(List*);
void print(List, FILE*);
void print(List);
Resource* CreateResource(int, int, char[], int, Resource*);
Resource* CreateResource(int, int, char[], int);
void getStatus(Resource*, FILE*);
void getStatus(List, FILE*);
void getStatus(List);
void prepend(List*, Resource*);
void append(List*, Resource*);
void insertSorted(List*, Resource*);
void FreeAll(List*);
List Read(FILE*);
void Write(List, FILE*);
void SaveBin(List, FILE*);
List LoadBin(FILE*);
//----------------------------------------------------------

void print(List list, FILE* file)
{
	for (Resource* r = list.head; r; r = r->next)
		printf("ID:%i Addr:%x Description%s Byte_Size:%i State:%i Next:%x\n",r->id,r->addr,r->description,r->byte_size,r->state,r->next);
}

void print(List list)
{
	print(list, stdout);
}

Resource* CreateResource(int id,int addr, char description[D], int byte_size, Resource* next)
{
	Resource* r = (Resource*)malloc(sizeof(Resource));
	if(!r)
	{
		r->state=0;
		fprintf(stderr,"ANy more resources cannot be created!\n");
		return NULL;
	}
	r->id=id;
	r->addr=addr;
	strncpy(r->description,description,D);
	r->byte_size=byte_size;
	r->state=1;
	r->next = next;
	return r;
}

Resource* CreateResource(int id,int addr, char description[D], int byte_size)
{
	return CreateResource(id,addr,description,byte_size, NULL);
}

void getStatus(Resource* resource, FILE* file)
{
	printf("ID:%i State:%i",resource->id,resource->state);
}

void getStatus(List list,FILE* file)
{
	for (Resource* r = list.head; r; r = r->next)
		getStatus(r,file);
}

void getStatus(List list)
{
	getStatus(list,stdout);
}

void prepend(List* list, Resource* resource)
{
	resource->next = list->head;
	list->head = resource;
}

void append(List* list, Resource* resource)
{
	if (list->head == NULL)
	{
		list->head = resource;
		return;
	}

	Resource* last = list->head;
	while (last->next)
		last = last->next;
	last->next = resource;
}

void insertSorted(List* list, Resource* resource) //insert when null and insert in the middle cases are done
//to be done: last when null +when not null and insert beginning when ist not null
{
	if (!list->head)
	{
		prepend(list, resource);
		return;
	}

	Resource* p = list->head;
	Resource* c = NULL;

	while (resource->addr > p->addr)
	{
		c = p;
		p = p->next;
	}

	c->next = resource;
	resource->next = p;
}

void FreeAll(List* list)
{
	for (Resource* r = list->head; r; r = r->next)
		free(r);
	free(list->head);
}

List Read(FILE* file)
{
	List list;
	initializeList(&list);

	int id,addr;
	char description[D];
	int byte_size;

	while(fscanf(file,"%i,%i,%s,%i\n",&id,&addr,description,&byte_size))
		append(&list,CreateResource(id,addr,description,byte_size));

	return list;
}

void Write(List list,FILE* file)
{
	for (Resource* r = list.head; r; r = r->next)
		fprintf(file,"%i,%i,%s,%i\n",r->id,r->addr,r->description,r->byte_size);
}

void SaveBin(List list,FILE* file)
{
	for (Resource* r = list.head; r; r = r->next)
		fwrite(r,sizeof(Resource),1,file);
}

List LoadBin(FILE* file)
{
	List list;
	initializeList(&list);
	Resource* r=NULL;
	while(fread(r,sizeof(Resource),1,file))
		append(&list,r);
	return list;
}

int main()
{
	// List list;
	// initializeList(&list);
	// prepend(&list, CreateResource(6));
	// prepend(&list, CreateResource(1));
	// append(&list, CreateResource(100));
	// insertSorted(&list, CreateResource(-1));
	// print(list);

	FILE* file=fopen("resources.csv","rb");
	List list=Read(file);
	getStatus(list);

	char c = getchar();
	return 0;
}