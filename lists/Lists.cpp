//basic implementation in cpp, with classes
//only prepend included (!append,!insert)
#include<stdio.h>
#include<stdlib.h>

class Node
{
	public:
		int val;
		Node* next;

		Node(int val)
		{
			next = (Node*)malloc(sizeof(Node));
			this->val = val;
			next->next = NULL;
		}

		static Node* CreateNode(int val, Node* next)
		{
			Node* node = (Node*)malloc(sizeof(Node));
			node->val = val;
			node->next = next;
			return node;
		}

		static Node* CreateNode(int val)
		{
			return CreateNode(val, NULL);
		}
};

class List
{
	public:
		Node* head;

		List(Node* head)
		{
			this->head = head;
		}

		List()
		{
			List(NULL);
		}

		void prepend(Node node)
		{
			node.next = head;
			head = &node;
		}

		void prepend(Node* node)
		{
			node->next = head;
			head = node;
		}

		void print(FILE* file)
		{
			for (Node* c = head; c; c = c->next)
				printf("%i ", c->val);
		}

		void print()
		{
			print(stdout);
		}
};

//int main()
//{
//	List list = List();
//	list.prepend(Node::CreateNode(6));
//	list.prepend(Node::CreateNode(5));
//	list.print();
//
//	char c = getchar();
//	return 0;
//}

