#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D 32
typedef struct Resource
{
	int id, addr;
	char description[D];
	int byte_size;
	unsigned int state : 1;
	struct Resource *next;
} Resource;

typedef struct List
{
	Resource *head;
} List;

void initializeList(List *list)
{
	list->head = NULL;
}

//----------------------------------------------------------
void initializeList(List *);
void print(List, FILE *);
void print(List);
Resource *CreateResource(int, int, char[], int, Resource *);
Resource *CreateResource(int, int, char[], int);
void getStatus(Resource *, FILE *);
void getStatus(List, FILE *);
void getStatus(List);
int getLength(List);
void prepend(List *, Resource *);
void append(List *, Resource *);
void insertSorted(List *, Resource *);
void Sort(List *);
void FreeAll(List *);
int CheckIntegrity(List);
void Optimize(List *);
List Read(FILE *);
void Write(List, FILE *);
void SaveBin(List, FILE *);
List LoadBin(FILE *);
//----------------------------------------------------------

void print(List list, FILE *file)
{
	for (Resource *r = list.head; r; r = r->next)
		printf("ID:%i Addr:%i Byte_Size:%i\n", r->id, r->addr, r->byte_size);
}

void print(List list)
{
	print(list, stdout);
}

Resource *CreateResource(int id, int addr, char description[D], int byte_size, Resource *next)
{
	Resource *r = (Resource *)malloc(sizeof(Resource));
	if (!r)
	{
		r->state = 0;
		fprintf(stderr, "ANy more resources cannot be created!\n");
		return NULL;
	}
	r->id = id;
	r->addr = addr;
	strncpy(r->description, description, D);
	r->byte_size = byte_size;
	r->state = 1;
	r->next = next;
	return r;
}

Resource *CreateResource(int id, int addr, char description[D], int byte_size)
{
	return CreateResource(id, addr, description, byte_size, NULL);
}

void getStatus(Resource *resource, FILE *file)
{
	printf("ID:%i State:%i", resource->id, resource->state);
}

void getStatus(List list, FILE *file)
{
	for (Resource *r = list.head; r; r = r->next)
		getStatus(r, file);
}

void getStatus(List list)
{
	getStatus(list, stdout);
}

int getLength(List list)
{
	int k = 0;
	for (Resource *r = list.head; r; r = r->next)
		k++;
	return k;
}

void prepend(List *list, Resource *resource)
{
	resource->next = list->head;
	list->head = resource;
}

void append(List *list, Resource *resource)
{
	if (list->head == NULL)
	{
		list->head = resource;
		return;
	}

	Resource *last = list->head;
	while (last->next)
		last = last->next;
	last->next = resource;
}

void insertSorted(List *list, Resource *resource) 
{
    Resource **current = &list->head;

    while (*current && resource->addr > (*current)->addr)
        current = &(*current)->next;

    resource->next = *current;
    *current = resource;
}

void Sort(List *list)
{
    List sorted;
    initializeList(&sorted);

    for (Resource *r = list->head; r; r = r->next)
        insertSorted(&sorted, CreateResource(r->id, r->addr, r->description, r->byte_size));

    FreeAll(list);
    *list = sorted;
}

void FreeAll(List *list)
{
	for (Resource *r = list->head; r; r = r->next)
		free(r);
	free(list->head);
}

int CheckIntegrity(List sorted)
{
	if (!sorted.head | !sorted.head->next)
		return 1;

	for (Resource *c = sorted.head; c->next; c = c->next)
		if ((c->addr + c->byte_size) > c->next->addr)
			return 0;
	return 1;
}

void Optimize(List *list)
{
    Sort(list);

    if (!list->head)
        return;

    Resource *last = list->head;
    for (Resource *c = list->head->next; c; c = c->next)
    {
        c->addr = last->addr + last->byte_size;
        last = c;
    }
}

List Read(FILE *file)
{
	List list;
	initializeList(&list);

	int id, addr;
	char description[D];
	int byte_size;

	while (fscanf(file, "%i,%x,\"%[^\"]\",%i\n", &id, &addr, description, &byte_size))
		append(&list, CreateResource(id, addr, description, byte_size));

	return list;
}

void Write(List list, FILE *file)
{
	for (Resource *r = list.head; r; r = r->next)
		fprintf(file, "%i,%i,%s,%i\n", r->id, r->addr, r->description, r->byte_size);
}

void SaveBin(List list, FILE *file)
{
	for (Resource *r = list.head; r; r = r->next)
		fwrite(r, sizeof(Resource), 1, file);
}

List LoadBin(FILE *file)
{
	List list;
	initializeList(&list);
	Resource *r = NULL;
	while (fread(r, sizeof(Resource), 1, file))
		append(&list, r);
	return list;
}

int main()
{
	List list;
	initializeList(&list);
	char message[32] = "ok";
	Resource *r1 = CreateResource(1, 100, message, 2);
	Resource *r2 = CreateResource(2, 1000, message, 10);
	Resource *r3 = CreateResource(3, 1001, message, 40);
	Resource *r4 = CreateResource(4, 1002, message, 12);
	insertSorted(&list, r1);
	insertSorted(&list, r2);
	insertSorted(&list, r3);
	insertSorted(&list, r4);
	print(list);
	printf("\n");
	Optimize(&list); // Call the Optimize function here

	print(list);

	// char c = getchar();
	// return 0;
}
