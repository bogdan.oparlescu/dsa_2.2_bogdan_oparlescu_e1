#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>

typedef struct Word
{
	char str[20];
	uint16_t occ;
	float fq;
}Word;


int GetIndex(char* s, int start_index, char wanted)
{
	for (int i = start_index; i < strlen(s); i++)
		if (s[i] == wanted)
			return i;
	return -1;
}

//void ReadWords(FILE* file)
//{
//	char* line = (char*)malloc(32);
//	Word* words = (Word*)malloc(sizeof(Word));
//	uint16_t lines_read = 0;
//	uint8_t line_index = 0;
//	char c;
//	while ((c = getc(file)) != EOF)
//	{
//		if (c == '\n')
//		{
//			line_index = 0;
//			lines_read++;
//			//...
//			printf("%s", line);
//			return;
//
//		}
//		line[line_index++] = c;
//	}
//
//}

Word* ReadWords(FILE* file,uint16_t* length)
{
	Word* words = (Word*)malloc(sizeof(Word));
	int lines_read = 0;
	Word word = { 0 };
	int i = 0;
	while (fscanf(file, "%[^,],%i,%f", word.str, &word.occ, &word.fq) != EOF)
	{
		words = (Word*)realloc(words, sizeof(Word)*(lines_read + 1));
		words[lines_read] = { "", word.occ, word.fq };
		strcpy(words[lines_read].str, word.str);
		lines_read++;
		////try to use strncpy here, ex: strncpy(&words[lines_read++],&word,size_sum);
		//strncpy((char*)&words[lines_read++], (char*)&words, sizeof(uint16_t) + sizeof(float) + 20);
	}
	*length = lines_read;
	return words;
}

void DumpC(FILE* file)
{
	char c;
	while ((c = getc(file)) != EOF)
		fputc(c, stdout);
}

int main()
{
	FILE* file = fopen("words.csv", "rb+");
	if (!file)
	{
		printf("File Error!\n");
		return -1;
	}
	uint16_t length = 0;
	Word* words = ReadWords(file, &length);
	for (int i = 0; i < length; i++)
		printf("%s %u %.2f\n", words[i].str, words[i].occ, words[i].fq);
}