#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>

typedef struct Word
{
	char str[20];
	uint16_t occ;
	float fq;
}Word;


int GetIndex(char* s, int start_index, char wanted)
{
	for (int i = start_index; i < strlen(s); i++)
		if (s[i] == wanted)
			return i;
	return -1;
}

Word* ReadWords(FILE* file, uint16_t* length)
{
	Word* words = (Word*)malloc(sizeof(Word));
	int lines_read = 0;
	Word word = { 0 };
	int i = 0;
	while (fscanf(file, "%[^,],%i,%f[^\n]", word.str, &word.occ, &word.fq) != EOF)
	{
		words = (Word*)realloc(words, sizeof(Word)*(lines_read + 1));
		words[lines_read] = { "", word.occ, word.fq };
		strcpy(words[lines_read].str, word.str);
		lines_read++;
	}
	*length = lines_read;
	return words;
}

void Print(Word* words, int length, int desc)
{
	if (!desc)
		for (int i = 0; i < length; i++)
			printf("%s", words[i].str);
	else
		for (int i = length - 1; i >= 0; i--)
			printf("%s", words[i].str);
}

int LowestFq(Word* words, int length)
{
	int lowest = words[0].fq;
	for (int i = 1; i < length; i++)
		if (lowest>words[i].fq)
			lowest = words[i].fq;
	return lowest;
}

int HighesttFq(Word* words, int length)
{
	int highest = words[0].fq;
	for (int i = 1; i < length; i++)
		if (highest<words[i].fq)
			highest = words[i].fq;
	return highest;
}

void PrintFq(Word* words, int length, int fq)
{
	for (int i = 0; i < length; i++)
		if (words[i].fq == fq)
			printf("%s", words[i].str);
	printf("\n");
}

int lsrc(Word* words, int length, char* str) //linear search
{
	for (int i = 0; i < length; i++)
		if (!strcmp(words[i].str, str))
			return i;
	return -1;
}

int bsrc(Word* words, int start_index, int end_index, char* str) //binary search
{
	int i = (end_index - start_index) / 2;
	int cond = strcmp(words[i].str, str);

	if (cond < 0)
		return bsrc(words, start_index, i, str);

	if (cond > 0)
		return bsrc(words, i, end_index, str);

	return i;
}

void PrintOcc(Word* words,int length, char* s)
{
	for (int i = 0; i < length; i++)
		if (!strcmp(words[i].str, s))
			printf("%s", words[i].str);
	printf("\n");
}

int main()
{
	FILE* file = fopen("words - sorted.csv", "rb+");
	if (!file)
	{
		printf("File Error!\n");
		return -1;
	}
	uint16_t length = 0;
	Word* words = ReadWords(file, &length);
	//printf("%i\n", lsrc(words, length, ""));
	//Print(words, length, 1);
}