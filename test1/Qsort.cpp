#include<stdio.h>
#include<stdlib.h>

void print(int*, int);

void Swap(int* a, int* b)
{
	*a ^= *b;
	*b ^= *a;
	*a ^= *b;
}

int Partition(int* A, int l, int r)
{
	int pivot = (l + r) / 2;
	int i = l;
	int j = r - 2;

	while (i <= j)
	{
		while (A[pivot] > A[i])
			i++;

		while (A[pivot] < A[j])
			j--;

		if (A[i]>A[j])
			Swap(&A[i], &A[j]);
	}
	Swap(&A[pivot], &A[i]);
	return i;
}


void _Qsort(int* A, int l, int r)
{
	if (l >= r)
		return;
	int index = Partition(A, l, r);
	_Qsort(A, l, index);
	_Qsort(A, index+1, r);
}

void Qsort(int* A, int length)
{
	_Qsort(A, 0, length);
}

void print(int* A, int length)
{
	for (int i = 0; i < length; i++)
		printf("%i ", A[i]);
	printf("\n");
}

int main()
{ 
	int A[] = { 2,31,42,54,101,-234,-7,-8};
	int length = sizeof(A) / sizeof(A[0]);
	//Qsort(A, length);
	//print(A, length);
	int k = Partition(A, 0, length);
	printf("%i\n", k);
	print(A, length);
}